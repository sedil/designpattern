#include <iostream>

class SteemWelcomer {
public:
    explicit SteemWelcomer(){
        std::cout << "welcome to your steemaccount." << "\n";
    }
};

class AccountNameCheck {
private:
    const std::string _userId = "username";
public:
    explicit AccountNameCheck(){ }

    bool checkUsername(const std::string &userId) const {
        if ( _userId.compare(userId) ){
            return true;
        } else {
            return false;
        }
    }

    std::string getUsername() const {
        return _userId;
    }
};

class PrivateKeyCheck {
private:
    const std::string _pk = "fw6vn68bneg5";
public:
    explicit PrivateKeyCheck (){ }

    bool checkPrivatekey(const std::string &pk) const {
        if ( _pk.compare(pk) ){
            return true;
        } else {
            return false;
        }
    }

    std::string getPrivateKey() const {
        return _pk;
    }
};

class FundsCheck {
private:
    int _steem = 90;
public:
    explicit FundsCheck(){ }

    int getCashInSteemAccount() const {
        return _steem;
    }

    void increaseSteemInAccount(const int value){
        _steem += value;
    }

    void decreaseSteemInAccount(const int value){
        _steem -= value;
    }

    bool haveEnoughSteem(const int steemToWithdraw){
        if ( steemToWithdraw > getCashInSteemAccount() ){
            std::cout << "Not enough steem : " << getCashInSteemAccount() << "\n";
            return false;
        } else {
            decreaseSteemInAccount(steemToWithdraw);
            std::cout << "Withdraw completed, New Balance : " << getCashInSteemAccount() << "\n";
            return true;
        }
    }

    void makeDeposit(const int steemToDeposit){
        increaseSteemInAccount(steemToDeposit);
        std::cout << "Deposit complete, New Balace : " << getCashInSteemAccount() << "\n";
    }
};

class SteemAccountFacade {
private:
    std::string _username, _privatekey;
    SteemWelcomer* _welcome;
    AccountNameCheck* _accname;
    PrivateKeyCheck* _pkcheck;
    FundsCheck* _funds;
public:
    explicit SteemAccountFacade(const std::string &username, const std::string &privatekey){
        _username = username;
        _privatekey = privatekey;

        _welcome = new SteemWelcomer;
        _accname = new AccountNameCheck;
        _pkcheck = new PrivateKeyCheck;
        _funds = new FundsCheck;
    }

    virtual ~SteemAccountFacade(){
        delete _welcome;
        delete _accname;
        delete _pkcheck;
        delete _funds;
    }

    std::string getAccountName() const {
        return _username;
    }

    std::string getPrivateKey() const {
        return _privatekey;
    }

    void withdrawSteem(int steemToGet){
        if (_accname->checkUsername(getAccountName()) ) {
            std::cout << "Transaction complete" << "\n";
        } else {
            std::cout << "Transaction failed" << "\n";
        }

        /*
        if (_accname->checkUsername(getAccountName()) &
            _pkcheck->checkPrivatekey(getPrivateKey()) &
            _funds->haveEnoughSteem(steemToGet) ){
            std::cout << "Transaction complete" << "\n";
        } else {
            std::cout << "Transaction failed" << "\n";
        }
        */
    }

    void depositSteem(int steemToDeposit){
        if (_accname->checkUsername(getAccountName()) &
            _pkcheck->checkPrivatekey(getPrivateKey()) ){

            _funds->makeDeposit(steemToDeposit);
            std::cout << "Transaction complete" << "\n";
        } else {
            std::cout << "Transaction failed" << "\n";
        }
    }
};

class SteemAccount {
public:
    explicit SteemAccount(){
        SteemAccountFacade accessAccount("username","fw6vn68bneg5");
        accessAccount.withdrawSteem(35);
        accessAccount.withdrawSteem(45);
        accessAccount.depositSteem(90);
    }
};
