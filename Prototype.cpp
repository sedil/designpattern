#include <iostream>

class NPC {
public:
    virtual ~NPC(){}
    virtual NPC* makeClone() = 0;
};

class Vendor : public NPC {
private:
    std::string _name;
public:
    Vendor(const std::string &name){
        _name = name;
        std::cout << "Create a Vendor with name : " << name << "\n";
    }

    NPC* makeClone(){
        Vendor* vendor = dynamic_cast<Vendor*>(this);
        std::cout << "Created a clone of : " << _name << "\n";
        return vendor;
    }

    std::string getVendorName() const {
        return _name;
    }
};

class Guard : public NPC {
private:
    std::string _name;
public:
    Guard(const std::string &name){
        _name = name;
        std::cout << "Create a Vendor with name : " << name << "\n";
    }

    NPC* makeClone(){
        Guard* vendor = dynamic_cast<Guard*>(this);
        std::cout << "Created a clone of : " << _name << "\n";
        return vendor;
    }

    std::string getGuardName() const {
        return _name;
    }
};

class CloneFactory {
public:
    NPC* getClone(NPC* npcSample){
        return npcSample->makeClone();
    }
};

class PrototypeTest {
public:
    explicit PrototypeTest(){
        CloneFactory npcMaker;
        Vendor* vendor = new Vendor("weapon seller of bruma");
        Vendor* clonedVendor = dynamic_cast<Vendor*>(npcMaker.getClone(vendor));

        std::cout << "Original : " << vendor->getVendorName() << "\n";
        std::cout << "Clone : " << clonedVendor->getVendorName() << "\n";

        delete vendor;
        delete clonedVendor;
    }
};
