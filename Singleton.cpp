class SinglePoint {
private:
    int _x, _y;

    // Konstruktoren und Destruktor nur private
    explicit SinglePoint() : _x(0), _y(0) {}
    explicit SinglePoint(const SinglePoint &cpy) = delete;
    SinglePoint& operator=(const SinglePoint &mv) = delete;

public:

    virtual ~SinglePoint(){}

    // Die einzige Instanz bekommen
    static SinglePoint& getInstance() {
        static SinglePoint *instance = new SinglePoint();
        return *instance;
    }

    void setCoord(int x, int y){
        _x = x;
        _y = y;
    }

    int* getCoord() const {
        int *coords = new int[2];
        *(coords) = _x;
        *(coords + 1) = _y;
        return coords;
    }
    // Aufruf : Point &p = Point::getInstance();
};
